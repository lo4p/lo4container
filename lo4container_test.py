from lo4container import Container

class Foo():
    def __init__(self):
        pass

def test_get_ok():
    c = Container()
    c['name'] = 'container'
    assert c['name'] =='container'

def test_call_function():
    c = Container()
    c['bar'] = lambda c: Foo()

    assert isinstance(c['bar'], Foo)

def test_no_share():
    c = Container()
    c['bar'] = lambda c: Foo()

    assert c['bar'] != c['bar']

def test_share():
    c = Container()
    c['bar'] = lambda c: Foo()
    c.share('bar')

    assert c['bar'] == c['bar']